Toms calc
================

### Installation manual

```sh
pipenv install --dev
python manage.py migrate
python manage.py loaddata retail_calculator/fixtures/initial.json
python manage.py runserver
```

### Runing tests
```sh
python manage.py test
```

### Usage

* Go to http://127.0.0.1:8000/ for use calculator
* For edit initial data (discounts and taxes) go to http://127.0.0.1:8000/admin/
