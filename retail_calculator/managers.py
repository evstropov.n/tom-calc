from decimal import Decimal

from django.db import models


class OrderDiscountQuerySet(models.query.QuerySet):

    def for_amount(self, amount: Decimal):
        return self.filter(order_sum_from__lte=amount).order_by('-order_sum_from').first()


class OrderDiscountManager(models.Manager):

    def get_queryset(self):
        return OrderDiscountQuerySet(model=self.model, using=self.db)

    def for_amount(self, amount: Decimal):
        return self.get_queryset().for_amount(amount)
