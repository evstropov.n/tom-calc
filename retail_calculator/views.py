from django.views.generic import FormView

from retail_calculator.forms import RetailCalcForm
from retail_calculator.services import calculate_order_cost


class RetailCalcFormView(FormView):
    template_name = 'retail_calculator/index.html'
    form_class = RetailCalcForm

    def form_valid(self, form):
        order_cost = calculate_order_cost(
            form.cleaned_data['item_amount'],
            form.cleaned_data['item_cost'],
            form.cleaned_data['state_code']
        )
        return self.render_to_response(self.get_context_data(**{'order_cost': order_cost}))
