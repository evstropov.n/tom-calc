from decimal import Decimal

from django.core.validators import MinValueValidator
from django.db import models

from retail_calculator.managers import OrderDiscountManager


class OrderDiscount(models.Model):
    order_sum_from = models.IntegerField(unique=True)
    discount = models.DecimalField(max_digits=4, decimal_places=2, validators=[MinValueValidator(Decimal('0.01'))])

    objects = OrderDiscountManager()

    def __str__(self):
        return f'>= {self.order_sum_from} ({self.discount} %)'

    class Meta:
        indexes = [
            models.Index(fields=['-order_sum_from']),
        ]


class CustomerState(models.Model):
    state_code = models.CharField(max_length=2, unique=True)
    tax_fee = models.DecimalField(max_digits=4, decimal_places=2, validators=[MinValueValidator(Decimal('0.00'))])

    def __str__(self):
        return f'{self.state_code} ({self.tax_fee} %)'
