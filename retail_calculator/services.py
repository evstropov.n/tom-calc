from decimal import Decimal

from retail_calculator.models import OrderDiscount, CustomerState


def calculate_order_cost(item_amount: int, item_price: Decimal, state: CustomerState) -> Decimal:
    total_sum = item_price * item_amount
    discount = OrderDiscount.objects.values_list('discount', flat=True).for_amount(total_sum)

    if discount and discount > 0:
        total_sum -= total_sum * (discount / 100)

    if state.tax_fee > 0:
        total_sum += total_sum * (state.tax_fee / 100)

    return total_sum.quantize(Decimal('0.00'))
