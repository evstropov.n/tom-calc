from django.contrib import admin

from retail_calculator.models import OrderDiscount, CustomerState


class CustomerStateAdmin(admin.ModelAdmin):
    list_display = ['id', 'state_code', 'tax_fee']


class OrderDiscountAdmin(admin.ModelAdmin):
    pass


admin.site.register(OrderDiscount, OrderDiscountAdmin)
admin.site.register(CustomerState, CustomerStateAdmin)
