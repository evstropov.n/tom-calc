from django import forms

from retail_calculator.models import CustomerState


class RetailCalcForm(forms.Form):
    item_amount = forms.IntegerField()
    item_cost = forms.DecimalField(decimal_places=2)
    state_code = forms.ModelChoiceField(
        queryset=CustomerState.objects.all().order_by('state_code'),
        to_field_name='state_code'
    )
