from decimal import Decimal

from django.test import TestCase
from parameterized import parameterized

from retail_calculator.models import CustomerState, OrderDiscount
from retail_calculator.services import calculate_order_cost


class CalculateOrderCostTestCase(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.state = CustomerState.objects.create(state_code='DD', tax_fee=Decimal('4.15'))
        cls.state_tax_free = CustomerState.objects.create(state_code='DE', tax_fee=Decimal('0'))
        cls.discount_3 = OrderDiscount.objects.create(order_sum_from='1000', discount=Decimal('3.00'))
        cls.discount_10 = OrderDiscount.objects.create(order_sum_from='10000', discount=Decimal('10.00'))

    @parameterized.expand([
        (3, Decimal('3.32'), Decimal('10.37')),
        (50, Decimal('4.22'), Decimal('219.76')),
        (100, Decimal('9.99'), Decimal('1040.46')),
    ])
    def test_without_discount(self, item_amount, item_price, result_price):
        result = calculate_order_cost(item_amount, item_price, self.state)

        self.assertEqual(result, result_price)

    @parameterized.expand([
        (100, Decimal('10'), Decimal('1010.26')),
        (10, Decimal('999.99'), Decimal('10102.45')),
        (10, Decimal('1000'), Decimal('9373.50')),
        (100, Decimal('10000'), Decimal('937350.00')),
    ])
    def test_with_discount(self, item_amount, item_price, result_price):
        result = calculate_order_cost(item_amount, item_price, self.state)

        self.assertEqual(result, result_price)

    def test_with_tax_free(self):
        result = calculate_order_cost(3, Decimal('3.32'), self.state_tax_free)

        self.assertEqual(result, Decimal('9.96'))
