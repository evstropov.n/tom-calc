from django.core.exceptions import ValidationError
from django.db.utils import IntegrityError
from django.test import TestCase

from retail_calculator.models import CustomerState, OrderDiscount


class CustomerStateTestCase(TestCase):

    def test_unique_state_code(self):
        CustomerState(state_code='DE', tax_fee='1').save()
        with self.assertRaisesMessage(IntegrityError, 'UNIQUE constraint failed'):
            CustomerState(state_code='DE', tax_fee='1').save()

    def test_out_of_range_tax_fee(self):
        with self.assertRaises(ValidationError):
            CustomerState(state_code='D3', tax_fee='100').clean_fields()

        with self.assertRaises(ValidationError):
            CustomerState(state_code='D3', tax_fee='-0.01').clean_fields()


class OrderDiscountTestCase(TestCase):

    def test_out_of_range_discount(self):
        with self.assertRaises(ValidationError):
            OrderDiscount(order_sum_from='100', discount='100').clean_fields()

        with self.assertRaises(ValidationError):
            OrderDiscount(order_sum_from='100', discount='0').clean_fields()
