from decimal import Decimal

from django.test import TestCase
from parameterized import parameterized

from retail_calculator.models import OrderDiscount


class OrderDiscountForAmountFilterTestCase(TestCase):

    @classmethod
    def setUpTestData(cls):
        OrderDiscount.objects.create(order_sum_from=Decimal('99'), discount=Decimal('1.11'))
        OrderDiscount.objects.create(order_sum_from=Decimal('100'), discount=Decimal('2.22'))
        OrderDiscount.objects.create(order_sum_from=Decimal('101'), discount=Decimal('3.33'))

    @parameterized.expand([
        (Decimal('89.99'), None),
        (Decimal('99.00'), Decimal('1.11')),
        (Decimal('99.99'), Decimal('1.11')),
        (Decimal('100.00'), Decimal('2.22')),
        (Decimal('100.99'), Decimal('2.22')),
        (Decimal('101.00'), Decimal('3.33')),
        (Decimal('10000'), Decimal('3.33')),
    ])
    def test_for_amount(self, order_sum, discount):
        result = OrderDiscount.objects.values_list('discount', flat=True).for_amount(order_sum)
        self.assertEqual(result, discount)
