from django.urls import path

from retail_calculator.views import RetailCalcFormView

urlpatterns = [
    path('', RetailCalcFormView.as_view()),
]
