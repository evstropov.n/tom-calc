from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class RetailCalculatorConfig(AppConfig):
    name = 'retail_calculator'
    verbose_name = _("Retail Calculator")
